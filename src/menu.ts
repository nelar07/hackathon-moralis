import { Authentication } from "./modules/Authentication";
import { Login } from "./modules/Login";
import { Signup } from "./modules/Signup";
import { File } from "./modules/File";
import { User } from "./modules/User";
import { FileIpfs } from "./modules/FileIpfs";

export const menu = [
  {
    path: "/authentication",
    label: "Authentication",
    component: Authentication,
  },
  {
    path: "/signup",
    label: "Signup",
    component: Signup,
  },
  {
    path: "/login",
    label: "Login",
    component: Login,
  },
  {
    path: "/user",
    label: "User",
    component: User,
  },
  {
    path: "/file-ipfs",
    label: "File (IPFS)",
    component: FileIpfs,
  },
 
 
];

import { extendTheme } from "@chakra-ui/react";

export const theme = extendTheme({
  config: {
    initialColorMode: "light",
  },
  components: {
    Input: {
      defaultProps: {
        variant: "filled",
      },
    },
    Button: {
      defaultProps: {
        colorScheme: "blue",
      },
    },
    Heading: {
      baseStyle: {
        marginBottom: "0.5em",
      },
    },
  },
});
